var SCREEN_WIDTH = window.innerWidth,
    SCREEN_HEIGHT = window.innerHeight,
    mouseX = 0, mouseY = 0,
    windowHalfX = window.innerWidth / 2,
    windowHalfY = window.innerHeight / 2,
    SEPARATION = 200,
    AMOUNTX = 10, AMOUNTY = 10,
    camera, scene, renderer;

var pointCloud = [
    [-1.0584308, 1.1667115, 0.0140898],
    [-0.2142359, -1.0452626, 0.0483488],
    [-0.2343216, -1.4357755, -0.0041353],
    [-0.919514, 0.0470648, 0.0032605],
    [-0.5015147, 0.0427323, -0.017004],
    [0.9836917, -1.1415041, 0.0438806],
    [0.0258696, 1.0632036, 0.0429934],
    [0.5335075, 1.3499586, 0.0196469],
    [0.0708345, 0.4663289, -0.0228047],
    [-1.7140965, 1.0337285, -0.0275303],
    [0.0796833, -1.9622503, 0.0340733],
    [-0.6048346, -0.9205381, 0.043184],
    [-2.101158, 1.1573525, -0.0381191],
    [-1.0959925, -0.5910232, 0.0409688],
    [1.3448019, -0.9203114, 0.0440311],
    [1.885692, 0.6484777, 0.0203254],
    [-0.5723988, -1.0187997, -0.0021898],
    [0.1223598, 1.5378786, 0.0175062],
    [1.4203919, -0.1472127, -0.0344098],
    [-0.1324234, -1.7643969, -0.0257401],
    [0.7698927, -0.7340648, 0.0204666],
    [-0.8776669, 0.6807007, -0.0407416],
    [1.9991058, 0.5962998, 0.0461753],
    [0.0205776, -1.6687644, -0.0228794],
    [-1.5672856, 1.5871891, 7.363E-4],
    [1.9022252, -0.7567402, 0.0311435],
    [-0.0348709, 0.4149234, 0.0233544],
    [1.9996481, 1.0866135, -0.0155239],
    [1.1278592, 0.94632, 0.0454045],
    [1.158638, 0.0193972, -0.0349976],
    [0.4267217, -1.0359768, 0.0354235],
    [-1.1109775, -0.4604269, -0.0449271],
    [0.2945019, -1.3688453, -0.0350778],
    [-0.3253991, 1.4522153, -0.0355146],
    [2.0186344, 0.7119075, 0.0479842],
    [0.9345925, -0.507712, 0.0487956],
    [-1.8857478, 0.3515571, 0.0236429],
    [-0.9264421, -0.7377753, -0.0296381],
    [-1.2731592, -0.3123529, -0.037453],
    [2.0299186, -1.0530161, 0.0356829]
];

init();
animate();

function init() {
    var container, separation = 100,
        amountX = 50, amountY = 50;

    container = document.createElement('div');
    document.body.appendChild(container);

    camera = new THREE.PerspectiveCamera( 75, SCREEN_WIDTH / SCREEN_HEIGHT, 0.01, 10000 );
    camera.position.set(0, 0, 10);

    scene = new THREE.Scene();
    scene.add( camera );

    renderer = new THREE.WebGLRenderer();
    renderer.setSize( SCREEN_WIDTH, SCREEN_HEIGHT );
    container.appendChild( renderer.domElement );

    // points
    for ( var i = 0; i < 40; i ++ ) {
	point = new THREE.Mesh( new THREE.CubeGeometry( 1, 1, 1 ),
                                   new THREE.MeshLambertMaterial( { color: 0x000000 }) );
	point.position.x = pointCloud[i][0] * 100;
	point.position.y = pointCloud[i][1] * 100;
	point.position.z = pointCloud[i][2] * 100;
	scene.add( point );
    }                     

    document.addEventListener( 'mousemove', onDocumentMouseMove, false );
    document.addEventListener( 'touchstart', onDocumentTouchStart, false );
    document.addEventListener( 'touchmove', onDocumentTouchMove, false );
}

function onDocumentMouseMove(event) {
    mouseX = event.clientX - windowHalfX;
    mouseY = event.clientY - windowHalfY;
}

function onDocumentTouchStart( event ) {
    if ( event.touches.length > 1 ) {
	event.preventDefault();

	mouseX = event.touches[ 0 ].pageX - windowHalfX;
	mouseY = event.touches[ 0 ].pageY - windowHalfY;
    }
}

function onDocumentTouchMove( event ) {
    if ( event.touches.length == 1 ) {
	event.preventDefault();

	mouseX = event.touches[ 0 ].pageX - windowHalfX;
	mouseY = event.touches[ 0 ].pageY - windowHalfY;
    }
}

function animate() {
    requestAnimationFrame( animate );
    render();
}

function render() {
    camera.position.x += ( mouseX - camera.position.x ) * .005;
    camera.position.y += ( - mouseY + 200 - camera.position.y ) * .005;
    camera.lookAt( scene.position );

    renderer.render( scene, camera );
}
