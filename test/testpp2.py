#!/usr/bin/env python

import unittest

class testPP2(unittest.TestCase):
    """
    A test class for norm approximation
    """

def suite():
    suite = unittest.TestSuite()
    suite.addTest(unittest.makeSuite(testPP2))
    return suite

if __name__ == "__main__":
    unittest.main()
