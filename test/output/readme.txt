The centroid of the point was use to determine sign of the normals
in the example output in this directory.  That is, as point p_i with
estimated normal n_i and centroid c, if  dot(p_i-c,n_i)<0 then 
n_i = -1*n_1.

The k-NN for each file:
10  pp2TestA0.txn
15  pp2TestA1.txn
20  pp2TestA2.txn
10  pp2TestB0.txn
15  pp2TestB1.txn
20  pp2TestB2.txn
10  pp2TestC0.txn
15  pp2TestC1.txn
20  pp2TestC2.txn
10  dolphins.txn
15  face.txn
20  bones.txn
15  v2.txn
20  bunny.txn
15  dragon.txn
15  world_curved.txn
5   stegosaurus.txn
