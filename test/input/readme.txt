
Example point cloud data files for pp2.  Example output of each
file in in the Output subdirectory.

pp2TestA?.txt is a ellipsoidal disk at the origin with major
     axis in x direction, minor axis in y direction, and small
     noise in z direction. A0 has 40 points, A1 has 400 points,
     and A2 has 800 points.
pp2TestB?.txt are the same points from TestA, but transformed by a rotation
     around z by pi/4, rotation around x by pi/6, rotation around y by pi/6,
     and finally a translation of [3,2,1].
pp2TextC?.txt are points on a unit hemishere with small amount of noise in 
     the normal direction.

The other files contains vertices from some typical models.  Warning: the bunny
and dragon models are large.

