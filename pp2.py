#!/usr/bin/env python

"""
Programming Problem #2
Dr. Heisterkamp
CS 3513, Section 801

Point Cloud Norm Approximation
"""

# written by Edward Cho
# last modified: 3/7/2012

from numpy import *
from numpy.linalg import svd
from sys import argv

def approximate_norm(p, c, k, points):
    # Find k nearest neighbors
    neighbors = [(dot(p - p2, p - p2), p2) for p2 in points]
    neighbors.sort(key = lambda t: t[0])
    nearest_neighbors = [n[1] for n in neighbors[:k+1]]

    # Approximate the norm with SVD
    centroid = mean(nearest_neighbors, 0)
    norm = svd(nearest_neighbors - centroid)[2][-1]

    # Estimate if the norm needs to be reversed
    if dot(p - c, norm) < 0:
        norm = -1 * norm

    return concatenate((p, norm))

if __name__ == "__main__":
    if len(argv) == 4:
        inputPath = argv[1]
        outputPath = argv[2]
        k = int(argv[3])

        # read input file
        points = loadtxt(inputPath)

        # centroid for all points
        # calculate once outside of the loop for sign heuristic
        c = mean(points, 0)

        # approximate norms for point cloud
        norms = [approximate_norm(p, c, k, points) for p in points]

        # write output file
        savetxt(outputPath, norms, fmt=['%.8g', '%.8g', '%.8g', '%.12g', '%.12g', '%.12g'])
    else:
        print "Usage: %s <input-file> <output-file> <k-nearest-neighbors>" % argv[0]
