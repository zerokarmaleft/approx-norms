#!/usr/bin/env python
""" Utility program to display the output of programming program 2, 
CS 3513, Spring 2012.

Needs module PyOpenGL and the glut libraries.

Saving a snapshot image requires the python imaging library, PIL.

Warning: just a quick throw-away program.  Not well tested.  Expect bugs.
"""

# written by Doug Heisterkamp
# last modified : 3/7/2012

from OpenGL.GL import *
from OpenGL.GLUT import *
from OpenGL.GLU import *
import sys

import numpy
import vtrackball


class DisplayStructure:
   """ Using glut, so we have ugly global data.  Creating a class
    to organize all of the global data for controling the display
    into one location.
   """
   pointColor = numpy.array([0.0,0.0,1.0])
   lineColor = numpy.array([1.0,0.0,0.0])
   pointSize = 3.0
   lineWidth = 1.0
   displayNormals = False
   displayPoints = True
   displayAxis = True
   normalScale = 0.07
   vt = vtrackball.VTrackball()
   boundingbox = numpy.array([[-1,-1,-1],[1,1,1]],float)
   centroid = numpy.array([0,0,0],float)
   doSpin = False
   timeStep = 50

def init():
   global dstruct
   pc = dstruct.pointColor
   glClearColor(1.0,1.0,1.0,1.0)
   glClearDepth(1.0)
   glEnable(GL_DEPTH_TEST)
   glColor3d(pc[0],pc[1],pc[2])
    

def display():
   global points
   global nlines
   global axlines
   global dstruct
   glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
   glMatrixMode(GL_MODELVIEW)
   glPushMatrix()
   glTranslated( dstruct.centroid[0], dstruct.centroid[1], dstruct.centroid[2])
   glMultMatrixd(dstruct.vt.currentRotation())
   glTranslated( -1*dstruct.centroid[0], -1*dstruct.centroid[1], -1*dstruct.centroid[2])
   glEnableClientState(GL_VERTEX_ARRAY)
   if dstruct.displayPoints:
      glColor3d( dstruct.pointColor[0], dstruct.pointColor[1], dstruct.pointColor[2])
      glVertexPointerd(points)
      glPointSize(dstruct.pointSize)
      glDrawArrays(GL_POINTS,0,points.shape[0])
   if dstruct.displayNormals:
      glColor3d( dstruct.lineColor[0], dstruct.lineColor[1], dstruct.lineColor[2])
      glVertexPointerd(nlines)
      glLineWidth(dstruct.lineWidth)
      glDrawArrays(GL_LINES,0,nlines.shape[0])
   if dstruct.displayAxis:
      glColor3d( 0.0, 1.0, 0.0)
      glVertexPointerd(axlines)
      glLineWidth(0.5)
      glDrawArrays(GL_LINES,0,axlines.shape[0])

   glPopMatrix()
   glutSwapBuffers()


def mouseClick(button,state,x,y):
   global dstruct
   if button==GLUT_LEFT_BUTTON and state == GLUT_DOWN:
      dstruct.vt.track(x,y,True)
   glutPostRedisplay()
 
def mouseMove(x,y):
   global dstruct
   dstruct.vt.track(x,y)
   glutPostRedisplay()
           
def keys(k,x,y):
   global points
   global nlines
   global dstruct
   update = False
   if k == b'\x1b' :  #esc key
      sys.exit(-1)
   elif k == b'r' :
      dstruct.doSpin = not dstruct.doSpin
      if dstruct.doSpin :
         glutTimerFunc(dstruct.timeStep, timer, 1 )
   elif k == b'p':
      dstruct.displayPoints = not dstruct.displayPoints
      update = True
   elif k == b'n':
      dstruct.displayNormals = not dstruct.displayNormals
      update = True
   elif k == b'+':
      dstruct.normalScale *= 1.2 
      makeNLines()
      update = True
   elif k == b'-':
      dstruct.normalScale *=0.8
      makeNLines()
      update = True
   elif k == b' ' :
      dstruct.doSpin = False
      dstruct.vt.clear()
      update = True
   elif k == b's' :
      ss = dstruct.vt.ScreenSize()
      tmpdata = glReadPixels(0, 0,ss[0],ss[1], GL_RGBA, GL_UNSIGNED_BYTE)
      try:
         import PIL.Image
         image = PIL.Image.fromstring("RGBA", ss, tmpdata)
         image.save('snapshot.png', 'PNG')
      except Exception as e :
         print("Failed to import python imaging library PIL or save image. Exception is {}".format(e))
   else:
      pass
   if update:
      glutPostRedisplay()


def menu(i):
   global dstruct
   update = False
   if i == 1:
      sys.exit(-1)
   elif i == 2:
      dstruct.displayPoints = not dstruct.displayPoints
      update = True
   elif i == 3:
      dstruct.displayNormals = not dstruct.displayNormals
      update = True
   elif i == 4:
      dstruct.doSpin = not dstruct.doSpin
      update = True
      if dstruct.doSpin :
         glutTimerFunc(dstruct.timeStep, timer, 1 )
   elif i == 5 :
      ss = dstruct.vt.ScreenSize()
      tmpdata = glReadPixels(0, 0,ss[0],ss[1], GL_RGBA, GL_UNSIGNED_BYTE)
      try:
         import PIL.Image
         image = PIL.Image.fromstring("RGBA", ss, tmpdata)
         image.save('snapshot.png', 'PNG')
      except Exception as e :
         print("Failed to import python imaging library PIL or save image. Exception is {}".format(e))
   elif i == 6 :
      dstruct.pointSize += 1
      update = True
   elif i == 7 :
      dstruct.pointSize = max(dstruct.pointSize -1 , 1)
      update = True
   else:
      pass
   if update:
      glutPostRedisplay()
   return i

def resize(w,h):
   global points
   global dstruct
   glViewport(0,0,w,h)
   glMatrixMode(GL_PROJECTION)
   glLoadIdentity()
   v = numpy.max(dstruct.boundingbox[1,:]- dstruct.boundingbox[0,:])
   minv = dstruct.centroid -v*(0.5 + 2*dstruct.normalScale) 
   maxv = dstruct.centroid +v*(0.5 + 2*dstruct.normalScale) 
   glOrtho(minv[0],maxv[0],minv[1],maxv[1],minv[2]-v,maxv[2]+v)
   glMatrixMode(GL_MODELVIEW)
   glLoadIdentity()
   dstruct.vt.setScreenSize(w,h)

def timer( *args ):
   global dstruct
   if dstruct.doSpin : 
      dstruct.vt.spin()
      glutTimerFunc(dstruct.timeStep, timer, 1 )
      glutPostRedisplay()

def makeNLines() :
   global points
   global normals
   global nlines
   global dstruct
   s = dstruct.normalScale*(numpy.max(dstruct.boundingbox)- numpy.min(dstruct.boundingbox))
   q = points + s*normals
   for i in range(points.shape[0]):
      nlines[2*i,:] = points[i]
      nlines[2*i+1,:] = q[i]
 
if __name__ == '__main__':
   if len(sys.argv)<2:
      print("need data file name on command line")
      sys.exit(-1)
   try:
      a = numpy.loadtxt(sys.argv[1])
   except Exception as e:
      print("Failed to read point-normal data from file {} with exception {}".format(sys.argv[1],e))
      sys.exit(-2)

   points = a[:,:3]
   normals = a[:,3:]
   nlines = numpy.row_stack((points,points))
   dstruct = DisplayStructure()
   dstruct.boundingbox = numpy.row_stack((numpy.min(points,0), numpy.max(points,0)))
   dstruct.centroid = numpy.sum(points,0)/points.shape[0]
   axlines = numpy.array([ [min(0,dstruct.boundingbox[0,0]),0,0], [max(0,dstruct.boundingbox[1,0]),0,0],
                           [0,min(0,dstruct.boundingbox[0,1]),0], [0,max(0,dstruct.boundingbox[1,1]),0],
                           [0,0,min(0,dstruct.boundingbox[0,2])], [0,0,max(0,dstruct.boundingbox[1,2])] ])
   glutInit(sys.argv)
   glutInitDisplayMode(GLUT_DOUBLE | GLUT_DEPTH | GLUT_RGB)
   glutInitWindowSize(700,700)
   glutCreateWindow("Point Cloud".encode('ascii'))
   glutDisplayFunc(display)
   glutMouseFunc(mouseClick)
   glutMotionFunc(mouseMove)
   glutReshapeFunc(resize)
   glutKeyboardFunc(keys)
   menuId = glutCreateMenu(menu)
   glutAddMenuEntry("Exit Program (esc)".encode('ascii'),1)
   glutAddMenuEntry("Toggle Point Display (p)".encode('ascii'),2)
   glutAddMenuEntry("Toggle Normal Display (n)".encode('ascii'),3)
   glutAddMenuEntry("Toggle Spinning".encode('ascii'),4)
   glutAddMenuEntry("Save snapshot image".encode('ascii'),5)
   glutAddMenuEntry("Increase Point Size".encode('ascii'),6)
   glutAddMenuEntry("Decrease Point Size".encode('ascii'),7)
   glutAttachMenu(GLUT_RIGHT_BUTTON)
   init()
   makeNLines()
   if dstruct.doSpin:
      glutTimerFunc(dstruct.timeStep, timer, 1 )
   glutEntryFunc(None)
   glutMainLoop()
