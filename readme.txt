Programming Problem #2 by Edward Cho
Dr. Heisterkamp
CS 3513, Section 801

=============== Requirements ===============

Python >= 2.7.2
numpy  >= 1.6.1

This program was developed on Mac OSX 10.7.3.

=============== Running      ===============

python pp2.py <infile> <outfile> <k-nearest-neighbors>

=============== Bugs          ===============

I implemented k-nearest-neighbors rather naively, sorting the entire
space for each outer loop, as I was prioritizing correctness over
optimizing performance for the larger datasets. The performance for
larger models is abysmal. Space partitioning or locality sensitive
hashing seems to be among the more clever algorithms for kNN. It seems
like memoization could also be used as a micro-optimization. I simply
ran out of time to improve this part of the algorithm.
