all:
	python pp2.py test/input/pp2TestA0.txt pp2TestA0.txn 10 &
	python pp2.py test/input/pp2TestA1.txt pp2TestA1.txn 15 &
	python pp2.py test/input/pp2TestA2.txt pp2TestA2.txn 20 &
	python pp2.py test/input/pp2TestB0.txt pp2TestB0.txn 10 &
	python pp2.py test/input/pp2TestB1.txt pp2TestB1.txn 15 &
	python pp2.py test/input/pp2TestB2.txt pp2TestB2.txn 20 &
	python pp2.py test/input/pp2TestC0.txt pp2TestC0.txn 10 &
	python pp2.py test/input/pp2TestC1.txt pp2TestC1.txn 15 &
	python pp2.py test/input/pp2TestC2.txt pp2TestC2.txn 20 &
	python pp2.py test/input/dolphins.txt dolphins.txn 10 &
	python pp2.py test/input/face.txt face.txn 15 &
	python pp2.py test/input/bones.txt bones.txn 20 &
	python pp2.py test/input/v2.txt v2.txn 15 &
	python pp2.py test/input/bunny.txt bunny.txn 20 &
	python pp2.py test/input/dragon.txt dragon.txn 15 &
	python pp2.py test/input/world_curved.txt world_curved.txn 15 &
	python pp2.py test/input/stegosaurus.txt stegosaurus.txn 5 &

clean:
	rm -f *.txn
